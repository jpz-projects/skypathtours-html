(function ($) {
	"use strict";

    jQuery(document).ready(function($){

        //Animation on Scroll
        let wow = new WOW(
            {
            boxClass:     'wow',      // default
            animateClass: 'animated', // default
            offset:       0,          // default
            mobile:       true,       // default
            live:         true        // default
          }
          )
        wow.init();

        //Counter Up
        // $('.count').counterUp({
        //     delay: 10,
        //     time: 1000
        // });
      
      //umra slider
      $("#umraSlider").slick({
        lazyLoad: "ondemand",
        prevArrow: "<i class='fas fa-arrow-left slick-left-arrow'></i>",
        nextArrow: "<i class='fas fa-arrow-right slick-right-arrow'></i>",
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 4000,
        infinite: true,
        autoplay: true,
      }); 
        
      //pacakge slider
      $("#pakageDetailsSlider").slick({
        lazyLoad: "ondemand",
        prevArrow: "<i class='fas fa-arrow-left slick-left-arrow'></i>",
        nextArrow: "<i class='fas fa-arrow-right slick-right-arrow'></i>",
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 4000,
        infinite: true,
        autoplay: true,
      });

    //Mobile Menu Active
    if ( $(window).width() <= 991 ) {
        $('.side-nav').on('click', function() {
            $('.main-menu').toggleClass('active');
        });
        $('.mobile-logo i').on('click', function() {
            $('.main-menu').removeClass('active');
        });
    }


    /********************************
     |*********Show Hide Navigation While Scroll*******|
    *******************************/

    ScrollNav();

    $(window).scroll(function(){
        ScrollNav();
    });
    function ScrollNav(){
            if( $(window).scrollTop() > 50 ){

                $(".site-header").addClass("scrolling");

                //scroll to top
                $(".button-scroll").fadeIn('slow');

            }else{
                $(".site-header").removeClass("scrolling");

                //scroll to top
                $(".button-scroll").fadeOut('slow');
            }
    }


    /********************************
	|*********Smooth Scroll to top*******|
	*******************************/
	$(".button-scroll").click(function (e) {
		e.preventDefault();

		var section_id = $(this).attr("href");

		$("html, body").animate({
			scrollTop: $(section_id).offset().top - 97
		}, 1000);
	});

	//scroll top
	if ($('.button-scroll').length) {
		$(window).scroll(function () {
			ScrollTop();
		});
		function ScrollTop() {
			if ($(window).scrollTop() > 400) {
				//scroll to top
				$(".button-scroll").addClass('active');
			} else {
				//scroll to top
				$(".button-scroll").removeClass('active');
			}
		}
	}



    });




}(jQuery));	